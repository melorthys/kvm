#!/bin/bash

qemu-system-x86_64 \
-boot menu=on \
-cpu host \
-smp 2 \
-cdrom ubuntu-20.04.2-desktop-amd64.iso \
-hda ubuntu.img \
-m 2 \
-vnc :0 \
-enable-kvm


