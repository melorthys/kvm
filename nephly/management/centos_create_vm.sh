#!/bin/bash

OS=$(dialog --clear --menu "Choose OS:" 0 0 6  \
1 "CentOS 8" \
2 "CentOS 7" \
3 "Ubuntu 20.04 LTS" \
4 "Ubuntu 18.04 LTS" \
5 "Ubuntu 16.10" \
6 "Debian 10" \
3>&1 1>&2 2>&3 3>&-)

case $OS in
1) OS="centos8.0" ;;
2) OS="centos7.0" ;;
3) OS="ubuntu20.04" ;;
4) OS="ubuntu18.04" ;;
5) OS="ubuntu16.10" ;;
6) OS="debian10" ;;
*)
esac

HOSTNAME=$(dialog --inputbox "Hostname:" 0 0 \
3>&1 1>&2 2>&3 3>&-)

MEMORY=$(dialog --radiolist "RAM:" 0 0 3 \
1 "512MB" ON \
2 "1G" OFF \
3 "2G" OFF \
3>&1 1>&2 2>&3 3>&-)

case $MEMORY in
1) MEMORY="512" ;;
2) MEMORY="1024" ;;
3) MEMORY="2048" ;;
*)
esac

CPU=$(dialog --radiolist "CPU:" 0 0 3 \
1 "" ON \
2 "" OFF \
3 "" OFF \
3>&1 1>&2 2>&3 3>&-)

FINAL=$(dialog --yesno \
"os   : $OS\n
name : $HOSTNAME\n
cpu  : $CPU\n
ram  : $MEMORY\n
\nWould you like to proceed?" 0 0 \
3>&1 1>&2 2>&3 3>&-)

clear

#if [ $FINAL -eq ??


DISTRO="centos"
IMG="centos7.qcow2"
OS_ID="centos7.0"
SIZE="10G"
MAIN_DIR="/var/lib/nephly"
USER_ID="1P0DoNqIKCRhM3CoiwvtEqAwfw0pQ9dI"
USER_DIR="$MAIN_DIR/users/$USER_ID"

#VM_NAME=$(shuf -zer -n32  {A..Z} {a..z} {0..9})
TIME=$(date +%F_%H:%I:%m%:::z)
VM_DIR="$USER_DIR/$HOSTNAME-$TIME"

mkdir $VM_DIR
cp $MAIN_DIR/config/cloud-init/$DISTRO.yaml $VM_DIR/
cloud-localds $VM_DIR/$OS_ID.iso $VM_DIR/$DISTRO.yaml
cp $MAIN_DIR/images/cloud/$IMG $VM_DIR/
qemu-img resize $VM_DIR/$IMG $SIZE

virt-install \
--name $HOSTNAME \
--memory $MEMORY \
--vcpus sockets=1,cores=1,threads=1 \
--cpu host \
--disk $VM_DIR/$IMG,device=disk,bus=virtio \
--disk $VM_DIR/$OS_ID.iso,device=cdrom \
--boot menu=on \
--os-type linux \
--os-variant $OS_ID \
--virt-type kvm \
--console pty,target_type=serial \
--graphics none \
--network bridge=virbr0,model=virtio \
--noautoconsole && virsh console $HOSTNAME

