#!/bin/bash

declare -a OperatingSystems=( \
"CentOS 8" \
"CentOS 7" \
"Ubuntu 20.04 LTS" \
"Ubuntu 18.04 LTS" \
"Ubuntu 16.10" \
"Debian 10" \
"Debian 9" \
)


index=0
input=$(
for os in "${OperatingSystems[@]}"; do
        while [ $index -le $(( ${#OperatingSystems[@]}-1 )) ]; do                
        index=$(( $index + 1 ));
                echo "$index" \""${OperatingSystems[$index-1]}"\";
    done
done)

echo $input

#dialog --clear --menu "Choose OS:" 0 0 7 $input 

#dialog --clear --menu "Test:" 0 0 1 1 "test"


#for os in "${OperatingSystems[@]}"; do
#       for index in {1..7}; do
#               echo "$index" "${OperatingSystems[$index-1]}";
#    done
#done

#cat images.json | jq '.images | select(type == "object")[] | .[].name' > images.txt

#cat images.txt | while read line; do
#    echo "Line contents are : $line"
#done


#length="${#OperatingSystems[@]}"

#for index in "${array[@]}"; do
#       echo "$index" 
#done

# Read each line and use regex parsing (with Bash's `=~` operator)
# to extract the value.
#while read -r line; do
  # Extract the value from between the double quotes
  # and add it to the array.
#  [[ $line =~ :[[:blank:]]+\"(.*)\" ]] && values+=( "${BASH_REMATCH[1]}" )
#done < "${array[@]}" 

#declare -p array # print the array
