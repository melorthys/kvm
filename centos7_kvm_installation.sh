#!/bin/bash

sudo yum -y update
sudo yum -y install firewalld
sudo systemctl start firewalld
sudo systemctl enable firewalld
sudo firewall-cmd --permanent --zone=puclic --remove-service=dhcpv6-client
sudo firewall-cmd --reload

# MAYBE OPTIONAL
sudo yum -y install vim
sudo yum -y install epel-release
sudo yum -y install htop

# Check if virtualization supported
lscpu | grep Virtualization

# Install kvm
sudo yum -y install qemu-kvm libvirt libvirt-python libguestfs-tools virt-install

# Start the libvirt service
sudo systemctl start libvirtd
sudo systemctl enable libvirtd

# Verify kvm module is loaded
lsmod | grep -i kvm


